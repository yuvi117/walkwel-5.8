<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('commented_by')->unsigned();
            $table->string('comment');
            $table->enum('status', [1,2,3])->comment = "1=active,2=hidden";
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('post_id')->references('id')->on('posts')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('commented_by')->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
