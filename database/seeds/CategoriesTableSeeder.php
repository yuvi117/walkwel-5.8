<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'              => 'Entertainment',
                'updated_at'        => now(),
                'created_at'        => now(),
            ],
            [
                'name'              => 'Politics',
                'updated_at'        => now(),
                'created_at'        => now(),
            ],
            [
                'name'              => 'Environmental',
                'updated_at'        => now(),
                'created_at'        => now(),
            ]
        ]);
    }
}
