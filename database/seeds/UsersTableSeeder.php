<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'              => 'Admin User',
                'email'             => 'admin123@yopmail.com',
                'password'          => \Hash::make(123456),
                'role'              => '1',
                'updated_at'        => now(),
                'created_at'        => now(),
            ],
            [
                'name'              => 'Author Singh',
                'email'             => 'author@yopmail.com',
                'password'          => \Hash::make(123456),
                'role'              => '2',
                'updated_at'        => now(),
                'created_at'        => now(),
            ],
            [
                'name'              => 'User Singh',
                'email'             => 'user@yopmail.com',
                'password'          => \Hash::make(123456),
                'role'              => '3',
                'updated_at'        => now(),
                'created_at'        => now(),
            ]
        ]);
    }
}
