<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('category_id')) {

            $cat = $request->category_id;
            $posts = Post::active()->unDeleted()->whereHas('category', function($q) use ($cat) {
                        $q->where('category_id', $cat);
                    });
        } else {

            $posts = Post::active()->unDeleted();
        }

        $posts = $posts->orderBy('id', 'desc')->paginate(2);

        $categories = Category::active()->unDeleted()->pluck('name', 'id');

        return view('posts.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::active()->unDeleted()->pluck('name', 'id');

        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'content'          => 'required|max:250|unique:posts,content,NULL,id,deleted_at,NULL',
            'category_id'      => 'required|integer|exists:categories,id',
        ];
           
        $messages = [
            'content.unique'        => 'Post already submitted',
            'category_id.exists'    => 'Select a valid category',
        ];

        $data = $request->validate($rules, $messages);
        $data['updated_by'] = auth()->user()->id;
        $data['posted_by'] = auth()->user()->id;

        $post = Post::create($data);

        return $this->successRedirect(route('posts.index') , 'Post has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {   
        return view('posts.view', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::active()->unDeleted()->pluck('name', 'id');

        return view('posts.edit', compact('categories', 'post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if ($post->posted_by != auth()->user()->id && auth()->user()->role != '1') {

            return $this->failRedirect(route('posts.index') , 'You don\'t have access to updated this post');
        }

        $rules = [
            'content'           => 'required|max:250|unique:posts,content,'.$post->id.',id,deleted_at,NULL',
            'category_id'       => 'required|integer|exists:categories,id'
        ];

        $messages = [
            'content.unique'        => 'Post already submitted',
            'category_id.exists'    => 'Select a valid category',
        ];

        $data = $request->validate($rules, $messages);

        $post->update($data);
        $data['updated_by'] = auth()->user()->id;

        return $this->successRedirect(route('posts.index') , 'Post has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
