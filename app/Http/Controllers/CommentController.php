<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'comment'               => 'required|max:250',
            'post_id'               => 'required|integer|exists:posts,id',
        ];
           
        $messages = [
            'post_id.exists'        => 'Select a valid Post',
        ];

        $data = $request->validate($rules, $messages);
        $data['commented_by'] = auth()->user()->id;

        $comment = Comment::create($data);

        return $this->successRedirect(route('posts.index') , 'Comment has been added successfully');
    }
}
