<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {	
    	if ( $request->method() == 'GET' ) {

    		return view('auth.login');

    	} else if ( $request->method() == 'POST' ) {

	    	$rules = [
	    		'email' 	=> 'required|email|exists:users,email|max:200',
	    		'password' 	=> 'required|min:6|max:16'
	    	];

	    	$messages = [
	    		'email.exists' => 'Please enter a registered email address',
	    	];

	    	$data = $request->validate($rules, $messages);

	    	$user = auth()->attempt($data, true);

	    	if ( $user ) {

	    		return redirect()->route('posts.index');

	    	} else {

	            return $this->failRedirect( route('login'), 'Please enter a valid email address or password');
	    	}
    	}
    }

    public function logout(Request $request)
    {
    	auth()->logout();
    	session()->flush();

    	return $this->successRedirect( route('login'), 'You have logged out successfully');
    }
}
