<?php

namespace App\Http\Middleware;

use Closure;
use App\Support\Responses\FlashAndRedirectResponse;

class CheckAdmin
{
    use FlashAndRedirectResponse;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if ($user) {

            if ($user->role == 1 || $user->role == 2) {

                return $next($request);
            }
        }

        return $this->failRedirect(route('posts.index'), 'You don\'t have access');
    }
}
