<?php

namespace App\Http\Middleware;

use Closure;
use App\Support\Responses\FlashAndRedirectResponse;

class CheckLogin
{
    use FlashAndRedirectResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if ($user ) {

            return $this->failRedirect(route('posts'), 'You are already login');
        }

        return $next($request);
    }
}
