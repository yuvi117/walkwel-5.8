<?php

namespace App;

use App\User;
use App\Comment;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = [
    	'category_id',
    	'posted_by',
    	'updated_by',
    	'content',
    	'status',
    	'deleted_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | Eloquent Relations
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function posted_by_user()
    {
        return $this->belongsTo(User::class, 'posted_by');
    }
    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /*
    |--------------------------------------------------------------------------
    | Query Scopes
    |--------------------------------------------------------------------------
    */
    public function ScopeActive($query)
    {
        return $query->whereStatus('1');
    }
    public function ScopeUnDeleted($query)
    {
        return $query->where('deleted_at', NULL);
    }
}
