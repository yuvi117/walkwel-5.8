<?php

namespace App;

use App\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
    	'comment',
        'post_id',
    	'commented_by',
    	'status',
    	'deleted_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | Eloquent Relations
    |--------------------------------------------------------------------------
    */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function commented_by_user()
    {
        return $this->belongsTo(User::class, 'commented_by');
    }

    /*
    |--------------------------------------------------------------------------
    | Query Scopes
    |--------------------------------------------------------------------------
    */
    public function ScopeActive($query)
    {
        return $query->whereStatus('1');
    }
    public function ScopeUnDeleted($query)
    {
        return $query->where('deleted_at', NULL);
    }
}
