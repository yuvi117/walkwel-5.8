@push('after-styles')
    <style type="text/css">
        .row.alert_msg {
            position: fixed;
            width: 40%;
            left: 30%;
            top: 10px;
            z-index: 999999;
        }
        .alert {
            padding: 9px;
            margin: 0px;
        }
    </style>
@endpush

@if($errors->any())
    <div class="row alert_msg">
        <div class="alert alert-danger text-center">
            <button class="close" data-close="alert"></button>
            <span>{!! $errors->first() !!}</span>
        </div>
        <div class="clearfix"></div>
    </div>
@endif

@if(Session::has('notification'))
    <div class="row alert_msg">
        <div class="alert alert-{{ Session::get('notification')['status'] == 'success' ? 'success' : 'danger'}}  text-center alert-dismissable">
            <span>{!! Session::get('notification')['message'] !!}</span>
        </div>
        <div class="clearfix"></div>
    </div>
@endif

@if (session()->get('success'))
    <div class="row alert_msg">
        <div class="alert alert-success text-center alert-dismissable">
            <span>{{ session()->get('success') }}</span>
        </div>
        <div class="clearfix"></div>
    </div>
@endif

@push('after-scripts')
    <script type="text/javascript">
        setTimeout(function() {
          $('.alert_msg').fadeOut('fast');
        }, 3000);
    </script>
@endpush
