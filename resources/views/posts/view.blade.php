@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">View Post
                    <a href="{{ route('posts.index') }}" style="float:right">Posts</a>
                </div>

                <div class="card-body">
                    @include('includes.alert-messages')

                    <b class="text-danger">Content :- </b> {{ ucfirst($post->content) }}<br>
                    <b>Posted By:-</b> {{ ucwords($post->posted_by_user->name) }}<br>
                    <b>Posted At:-</b> {{ $post->created_at->format('F d, Y h:i a') }}<br><br>

                    @forelse($post->comments as $comment)

                        {!! '<b>Comment '.$loop->iteration.' :- </b>'. $comment->comment . '<br>' !!}
                        {!! '<b>Commented By :- </b> '. $comment->commented_by_user->name . '<br><br>' !!}
                    @empty
                        <h4 class="text-center text-danger"><b>No Comments </b></h4>

                    @endforelse

                    {!! Form::open(['route' => ['comment.store'], 'class' => 'form-horizontal', 'method' => 'post', 'autocomplete' => 'off']) !!}

                        @auth
                        <label for="com">Comment</label>
                        {!! Form::textarea('comment', null, ['class'=>'form-control', 'id' => 'com', 'rows' => 2, 'cols' => 35]) !!}

                        {!! Form::hidden('post_id', $post->id) !!}

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <a href="{{ route('posts.index') }}" class="btn btn-white" >Cancel</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                        @endauth

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
