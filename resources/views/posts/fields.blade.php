
<div class="form-group">
    <label class="col-sm-2 control-label">Category</label>
    <div class="col-sm-10">
		{!! Form::select('category_id', $categories, null , ['class' => 'form-control chosen-select', 'placeholder' => 'Select Category', 'required' => true]) !!}
	</div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Content</label>
    <div class="col-sm-10">
        {!! Form::text('content', old('content'), ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Content']) !!}
    </div>
</div>