@extends('layouts.app')

@push('after-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('posts.index') }}" style="float:left">All Posts</a>
                    @auth
                    <a href="{{ route('posts.create') }}" style="float:right">Create</a>
                    @endauth
                </div>

                <div class="card-body">
                    @include('includes.alert-messages')

                    {!! Form::open(['route' => ['posts.index'], 'class' => 'form-horizontal', 'method' => 'post', 'autocomplete' => 'off', 'id' => 'cat-form']) !!}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-4">
                                {!! Form::select('category_id', $categories, null , ['class' => 'form-control chosen-select', 'placeholder' => 'Select Category', 'required' => true, 'id' => 'cat']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}

                    <table class="table">
                    <thead>
                      <tr>
                        <th>Category</th>
                        <th>Content</th>
                        <th>Posted By</th>
                        <th>Posted At</th>
                        @auth
                        <th>Action</th>
                        @endauth
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                          <tr>
                            <td>{{ ucwords($post->category->name) }}</td>
                            <td>{{ $post->content }}</td>
                            <td>{{ ucwords($post->posted_by_user->name) }}</td>
                            <td>{{ $post->created_at->format('F d, Y h:i a') }}</td>
                            <td>
                                @auth
                                @if($post->posted_by == auth()->user()->id || auth()->user()->role == '1')
                                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                @endif
                                @endauth
                                <a href="{{ route('posts.show', $post->id) }}" class="btn btn-success btn-sm" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>

                            </td>
                          </tr>
                        @endforeach
                    </tbody>
                  </table>

                  {{ $posts->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
           $('#cat').change(function(){

               $('#cat-form').submit();
            });
        });
    </script>
@endpush