@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Post
                    <a href="{{ route('posts.index') }}" style="float:right">Posts</a>
                </div>

                <div class="card-body">
                    @include('includes.alert-messages')
                    {!! Form::open(['route' => ['posts.store'], 'class' => 'form-horizontal', 'method' => 'post', 'autocomplete' => 'off']) !!}

                        @include('posts.fields')

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <a href="{{ route('posts.index') }}" class="btn btn-white" >Cancel</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
