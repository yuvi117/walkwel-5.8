<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Pages without login*/
Route::group(['middleware' => ['check.login']], function(){

    Route::match(['get', 'post'], 'login', 'LoginController@login')->name('login');
});
   
/*Pages without login*/
Route::match(['get', 'post'], 'posts', 'PostController@index')->name('posts.index');
Route::get('/', 'PostController@index')->name('landing');
Route::get('posts/{post}', 'PostController@show')->name('posts.show');

/*Pages for Admin and Author with login*/
Route::group(['middleware' => ['check.admin']], function(){

    Route::get('post/create', 'PostController@create')->name('posts.create');
    Route::post('posts/create', 'PostController@store')->name('posts.store');
    Route::put('posts/{post}/edit', 'PostController@update')->name('posts.update');
    Route::get('posts/{post}/edit', 'PostController@edit')->name('posts.edit');


});

/*Pages for Admin and Author with login*/
Route::group(['middleware' => ['auth']], function(){
    Route::post('comment', 'CommentController@store')->name('comment.store');
    Route::post('logout', 'LoginController@logout')->name('logout');
});